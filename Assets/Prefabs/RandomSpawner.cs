using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using TMPro;
using System;

public class RandomSpawner : MonoBehaviour
{
    public float wait;
    private float timer = 0.0f;
    public GameObject redBaloonPrefab;
    public GameObject blueBaloonPrefab;
    public GameObject greenBaloonPrefab;
    public GameObject purpleBaloonPrefab;
    public GameObject yellowBaloonPrefab;

    public GameObject jumpscareObject; // The 'scary' object to spawn
    public int numberOfObjects; // The number of objects to spawn

    private bool jumpscare = false;

    public Camera camera;

    private GameObject[] pool;

    private int score;
    public TextMeshProUGUI scoreText;

    void Start()
    {
        pool = new GameObject[] { redBaloonPrefab, blueBaloonPrefab, greenBaloonPrefab, purpleBaloonPrefab, yellowBaloonPrefab };
        score = 0;
        scoreText.text = "Score: " + score;
    }

    public void UpdateScore(bool jumpscare)
    {
        this.jumpscare = jumpscare;
        score += 1;
        scoreText.text = "Score: " + score;
    }

    void showJumpscare()
    {
        if (jumpscare)
        {
            for (int i = 0; i < numberOfObjects; i++)
            {

                Vector3 spawnPoint;
                while (true)
                {
                    spawnPoint = new Vector3(UnityEngine.Random.Range(-30, 31), UnityEngine.Random.Range(1, 5), UnityEngine.Random.Range(-30, 31));
                    float dist = Vector3.Distance(spawnPoint, this.transform.position);

                    if (dist > 3.0f) break;
                }

                // Instantiate the object
                GameObject instance = Instantiate(jumpscareObject, spawnPoint, Quaternion.identity);

                Vector3 directionToCamera = (camera.transform.position - instance.transform.position).normalized;
                // Rotate the object to face the camera
                instance.transform.rotation = Quaternion.LookRotation(directionToCamera);
                Destroy(instance, 3f);

                Destroy(instance, 3f);

            }
            jumpscare = false;
        }
    }
    void Update()
    {
        timer += Time.deltaTime;

        if (timer > wait)
        {
            Vector3 spawnPoint;
            while (true)
            {
                spawnPoint = new Vector3(UnityEngine.Random.Range(-30, 31), UnityEngine.Random.Range(1, 5), UnityEngine.Random.Range(-30, 31));
                float dist = Vector3.Distance(spawnPoint, this.transform.position);

                if (dist > 3.0f) break;
            }

            Instantiate(pool[UnityEngine.Random.Range(0, 6)], spawnPoint, Quaternion.identity);

            timer = timer - wait;
        }

        showJumpscare();

    }
}
