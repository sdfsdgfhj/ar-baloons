using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using UnityEngine;

public class TouchPop : MonoBehaviour
{
    public Camera arCamera;
    public RandomSpawner spawner;


    void Update()
    {
        if (Input.touchCount == 0)
            return;

        Touch touch = Input.GetTouch(0);

        Ray ray = arCamera.ScreenPointToRay(touch.position);
        RaycastHit hit;

        if (Physics.Raycast(ray, out hit))
        {
            Destroy(hit.transform.gameObject);
            bool jumpscare = false;
            if (UnityEngine.Random.Range(0, 10) == 0)
            {
                jumpscare = true;
            }
            spawner.UpdateScore(jumpscare);
        }
    }




}
